# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = str.split(" ")
  hsh = Hash.new(0)
  words.each {|word| hsh[word] = word.length}
  hsh
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.index(hash.values.max)
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  # latest = older.merge(newer)
  newer.each {|gems, amount| older[gems] = amount}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  wordhsh = Hash.new(0)
  word.chars.each do |char|
    wordhsh[char] +=1
  end
  wordhsh
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hsh = Hash.new(0)
  arr.each {|el| hsh[el] += 1}
  hsh.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hsh = Hash.new(0)
  numbers.each do |el|
      if el.even?
        hsh[:even] += 1
      else
        hsh[:odd] += 1
      end
  end
  hsh
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowelhash = {a:0, e:0, i:0, o:0, u:0}
  string.chars.each do |letter|
    if "aeiou".include?(letter)
      vowelhash[letter.to_sym] += 1
    end
  end
  vowelhash.index(vowelhash.values.max).to_s
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  secondhalf = students.select {|k,v| v >= 7}
  names = secondhalf.keys.combination(2).to_a
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species = Hash.new(0)
  specimens.each {|el| species[el] += 1}
  #number_of_species          smallest pop           largest pop
  species.keys.length ** 2 * (species.values.min  / species.values.max)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  oldfreq = Hash.new(0)
  vandalfreq = Hash.new(0)
  charsonlyold = prep_string(normal_sign)
  charsonlynew = prep_string(vandalized_sign)
  charsonlyold.each {|el| oldfreq[el.downcase] += 1}
  charsonlynew.each {|el| vandalfreq[el.downcase] += 1}

  vandalfreq.each do |letter, freq|
    unless oldfreq.include?(letter) && freq <= oldfreq[letter]
      return false
    end
  end
  true
end

def prep_string(str)
  str.delete!("!'., ")
  str.split('')
end
